import { TypeScript, TypeScriptOptions } from "./lib/plugin";
declare var _default: ((inputNode: any, options?: TypeScriptOptions | undefined) => any) & {
    TypeScript: typeof TypeScript;
    typescript: (inputNode: any, options?: TypeScriptOptions | undefined) => TypeScript;
};
export = _default;
