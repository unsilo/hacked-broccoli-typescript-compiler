import { NormalizedOptions, TypeScriptPluginOptions } from "./interfaces";
export default function normalizeOptions(options: TypeScriptPluginOptions, inputPath: string): NormalizedOptions;
